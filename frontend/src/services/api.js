const url = 'http://localhost:3001/api/'

async function  getAllCharacter() {
  return await fetch(`${url}characters`)
    .then(res => res.json())
    .then(res => res.result)
}

async function getCharacterById(id) {
  return await fetch(`${url}characters/${id}`)
    .then(res => res.json())
    .then(res => res.result)
}

export default {
  getAllCharacter,
  getCharacterById,
}
