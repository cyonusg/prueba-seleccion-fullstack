'use strict';

const server = require('./config/express');
const mongoonseInit = require('./config/mongoose');
const initData = require('./config/loadData');

server();
mongoonseInit();
initData();
