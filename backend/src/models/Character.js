'use strict';

const { Schema, model } = require('mongoose');

const characterSchema = new Schema({
  titles: { type: Array, required: true },
  books: { type: Array, required: true },
  house: { type: String, required: false },
  slug: { type: String, required: true },
  name: { type: String, required: true },
});

module.exports = model('Character', characterSchema, 'character');
