'use strict';

const express = require('express');
const { getAll, get } = require('../controllers/character');

const router = express.Router();

router.get('/test', (req, res, next) => {
  return res.json({ result: 'Hello' });
});

router.get('/characters', getAll);
router.get('/characters/:id', get);

module.exports = router;
