'use strict';
const Character = require('../models/Character');
const { apiResponse } = require('../utils/common');
const fetch = require("node-fetch");

const loadData = async () => {
    const haveData = await checkData();

    if(haveData.length == 1) {
        console.log('King in The North',haveData);
        return 
    }

    const characters = await 
    fetch('https://api.got.show/api/map/characters')
        .then(response => response.json())
        .then(data => data.data);

    saveInitData(characters)
}

const saveInitData = async (characters) => {
    characters.forEach(async (item) => {
        try {
            const character = new Character({
                titles:item.titles,
                books:item.books,
                house:item.house,
                slug:item.slug,
                name:item.name
            });
            await character.save();
        } catch(error) {
            console.log(error);
        }
    });
    console.log('New king in the North (init data loaded)');
}


const checkData = async () => {
    try {
        const Characters = await Character.find().limit(1);
        return Characters;
    } catch(error) {
        return next(error);
    }
}

module.exports = loadData;