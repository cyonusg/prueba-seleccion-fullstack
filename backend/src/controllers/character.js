'use strict';

const Character = require('../models/Character');
const { apiResponse } = require('../utils/common');

const getAll = async (req, res, next) => {
  try {
    const Characters = await Character.find();
    return res.json(apiResponse(Characters));
  } catch(error) {
    return next(error);
  }
}

const get = async (req, res, next) => {
  try {
    const character = await Character.findById(req.params.id);
    return res.json(apiResponse(character));

  } catch(error) {
    return next(error);
  }
}

module.exports = {
  getAll,
  get
}
